package com.obitrend.app.helpers;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.obitrend.app.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ekirapa on 4/25/18.
 * Class with picasso methods
 */

public class PicasoHelper {
    public static Picasso getPicaso(final Context context) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + new SessionManager(context).getKeyBearerToken())
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();

        return new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();
    }
}
