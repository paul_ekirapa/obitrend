package com.obitrend.app.helpers;

import android.util.Log;

import com.obitrend.app.utils.UrlConfig;

/**
 * Created by ekirapa on 4/24/18.
 * Class to map image urls
 */


public class UrlMapper {
    public static String imageUrlMapper(String url) {
        String ip = "http://obitrend.com/api/storage/upload/";
        String mapped_url = url.substring(url.lastIndexOf('/') + 1);
        return ip + mapped_url;
    }
}
