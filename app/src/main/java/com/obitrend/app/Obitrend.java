package com.obitrend.app;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.obitrend.app.models.AppDatabase;
import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ekirapa on 4/20/18.
 * Application class
 */

public class Obitrend extends Application {
    private static Obitrend mInstance;

    public static synchronized Obitrend getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        FlowManager.init(FlowConfig.builder(this).addDatabaseConfig(DatabaseConfig.builder(AppDatabase.class)
                .databaseName("Obitrnd_DB")
                .build())
                .build());
        Fabric.with(this, new Crashlytics());
        logUser();

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SessionManager sessionManager = new SessionManager(this);
        if (sessionManager.getUserName() != null) {
            Crashlytics.setUserIdentifier(sessionManager.getUserName());
            Crashlytics.setUserName(sessionManager.getUserName());
        }
    }

}
