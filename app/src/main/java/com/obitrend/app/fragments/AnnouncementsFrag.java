package com.obitrend.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.obitrend.app.Activities.AnnouncementItem;
import com.obitrend.app.R;
import com.obitrend.app.adapters.AnnouncementsAdapter;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.utils.AppConstants;
import com.obitrend.app.utils.RecyclerItemClickListener;

/**
 * Created by ekirapa on 4/9/18.
 * Announcement fragment class
 */

public class AnnouncementsFrag extends Fragment {
    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_layout, container, false);
        registerViews();
        return v;
    }

    private void registerViews() {
        RecyclerView recyclerView = v.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        final AnnouncementsAdapter adapter = new AnnouncementsAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Announcement announcement = adapter.getAnnouncement(position);
                Intent intent = new Intent(getContext(), AnnouncementItem.class);
                Bundle b = new Bundle();
                b.putParcelable(AppConstants.ANNOUNCEMENT, announcement);
                intent.putExtras(b);
                startActivity(intent);
            }
        }));
    }
}
