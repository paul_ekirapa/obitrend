package com.obitrend.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.hbb20.CountryCodePicker;
import com.obitrend.app.Activities.MakeRequest;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.utils.FragmentModel;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ekirapa on 5/8/18.
 */

public class AnnouncementDetails extends Fragment {
    private View v;
    EditText title, description, days, location;
    String type = "";
    String country = "KE";
    String user_id = "3";
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_details, container, false);
        registerViews();
        return v;
    }

    private void registerViews() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Submitting Request");
        progressDialog.setCancelable(false);

        title = v.findViewById(R.id.et_title);
        description = v.findViewById(R.id.et_description);
        days = v.findViewById(R.id.et_days);
        location = v.findViewById(R.id.et_location);

        Spinner spinner = (Spinner) v.findViewById(R.id.spinner_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final CountryCodePicker countryCodePicker = v.findViewById(R.id.ccp);
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                country = countryCodePicker.getSelectedCountryNameCode();
            }
        });
        v.findViewById(R.id.tv_request).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check()) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("user_id", user_id);
                        jsonObject.put("type_of_announcement", type);
                        jsonObject.put("description", description.getText().toString());
                        jsonObject.put("country", country);
                        jsonObject.put("location", location.getText().toString());
                        jsonObject.put("payment", "KI843");
                        jsonObject.put("days", days.getText().toString());
                        jsonObject.put("is_featured", "1");
                        jsonObject.put("status", "1");
                        jsonObject.put("title", title.getText().toString());

                        if (getActivity() != null) {
                            ((MakeRequest) getActivity()).getRequest().setUser_id(user_id);
                            ((MakeRequest) getActivity()).getRequest().setType_of_announcement(type);
                            ((MakeRequest) getActivity()).getRequest().setDescription(description.getText().toString());
                            ((MakeRequest) getActivity()).getRequest().setCountry(country);
                            ((MakeRequest) getActivity()).getRequest().setLocation(location.getText().toString());
                            ((MakeRequest) getActivity()).getRequest().setDays(days.getText().toString());
                            ((MakeRequest) getActivity()).getRequest().setIs_featured("0");
                            ((MakeRequest) getActivity()).getRequest().setTitle(title.getText().toString());
                        }

                        //makeRequest(jsonObject);
                        FragmentModel.getInstance().addNewFrag(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private boolean check() {
        if (title.getText().toString().isEmpty()) {
            title.setError("Enter title");
            return false;
        } else if (description.getText().toString().isEmpty()) {
            description.setError("Enter the description");
            return false;
        } else if (days.getText().toString().isEmpty()) {
            days.setError("Enter number of days");
            return false;
        } else if (location.getText().toString().isEmpty()) {
            location.setError("Enter location.");
            return false;
        } else if (type.isEmpty()) {
            Toast.makeText(getContext(), "Choose an announcement type", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void makeRequest(final JSONObject jsonObject) {
        final SessionManager sessionManager = new SessionManager(getContext());
        progressDialog.show();
        Log.d("JSON", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlConfig.MAKE_REQUEST, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.has("success")) {
                    Toast.makeText(getContext(), "Request completed", Toast.LENGTH_SHORT).show();
                }
                completeRequest();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                closeProgressbar();
                Toast.makeText(getContext(), "Cant submit request.Try again", Toast.LENGTH_LONG).show();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                return headers;
            }
        };
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);


    }

    private void completeRequest() {
        try {
            getActivity().finish();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
