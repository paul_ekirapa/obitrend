package com.obitrend.app.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.obitrend.app.Activities.MakeRequest;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.models.AnnouncementRequest;
import com.obitrend.app.utils.FragmentModel;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ekirapa on 5/9/18.
 * <p>
 * Upload files fragment
 */

public class AnnouncementFiles extends Fragment {
    private View v;
    private ImageView pic, passport, files;
    Image pic_image, passport_image, file_image;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_files, container, false);
        registerViews();
        return v;
    }

    private void registerViews() {
        pic = v.findViewById(R.id.img_pic);
        passport = v.findViewById(R.id.img_passport);
        files = v.findViewById(R.id.img_file);

        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(AnnouncementFiles.this)
                        .returnMode(ReturnMode.ALL)
                        .folderMode(true)
                        .single()
                        .toolbarFolderTitle("Folder")
                        .toolbarImageTitle("Tap to select")
                        .start(1);
            }
        });

        passport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(AnnouncementFiles.this)
                        .returnMode(ReturnMode.ALL)
                        .folderMode(true)
                        .single()
                        .toolbarFolderTitle("Folder")
                        .toolbarImageTitle("Tap to select")
                        .start(0);
            }
        });

        files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(AnnouncementFiles.this)
                        .returnMode(ReturnMode.ALL)
                        .folderMode(true)
                        .single()
                        .toolbarFolderTitle("Folder")
                        .toolbarImageTitle("Tap to select")
                        .start(2);
            }
        });

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Submiting request");

        v.findViewById(R.id.tv_request).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passport_image == null || pic_image == null || file_image == null) {
                    Toast.makeText(getContext(), "Pic a all image files", Toast.LENGTH_LONG).show();

                } else {
                    uploadFiles();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<Image> images = ImagePicker.getImages(data);
        if (images != null && !images.isEmpty()) {
            if (requestCode == 0) {
                passport.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
                passport.setScaleType(ImageView.ScaleType.CENTER_CROP);
                passport_image = images.get(0);
            } else if (requestCode == 1) {
                pic.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
                pic.setScaleType(ImageView.ScaleType.CENTER_CROP);
                pic_image = images.get(0);

            } else if (requestCode == 2) {
                files.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
                files.setScaleType(ImageView.ScaleType.CENTER_CROP);
                file_image = images.get(0);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadFiles() {
        if (getActivity() != null) {
            progressDialog.show();
            AnnouncementRequest request = ((MakeRequest) getActivity()).getRequest();
            final SessionManager sessionManager = new SessionManager(getContext());
            SimpleMultiPartRequest partRequest = new SimpleMultiPartRequest(Request.Method.POST, UrlConfig.MAKE_REQUEST, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("response", response);
                    if (response.contains("success")) {
                        Toast.makeText(getContext(), "Request submitted successfully", Toast.LENGTH_SHORT).show();
                    }
                    closeProgressbar();
                    getActivity().finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    closeProgressbar();
                    Toast.makeText(getContext(), "Cant submit request now.Try again", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                    return headers;
                }
            };
            partRequest.addStringParam("user_id", "3");
            partRequest.addStringParam("type_of_announcement", request.getType_of_announcement());
            partRequest.addStringParam("description", request.getDescription());
            partRequest.addStringParam("country", request.getCountry());
            partRequest.addStringParam("location", request.getLocation());
            partRequest.addStringParam("payment", "KHF7H");
            partRequest.addStringParam("days", request.getDays());
            partRequest.addStringParam("is_featured", "0");
            partRequest.addStringParam("status", "0");
            partRequest.addStringParam("title", request.getTitle());
            partRequest.addFile("image_path", passport_image.getPath());
            if (pic_image != null)
                partRequest.addFile("image_thumb", pic_image.getPath());
            if (file_image != null)
                partRequest.addFile("file_path", file_image.getPath());
            VolleySingleton.getInstance(getContext()).addToRequestQueue(partRequest);

        }
    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
