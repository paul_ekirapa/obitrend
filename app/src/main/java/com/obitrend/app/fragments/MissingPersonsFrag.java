package com.obitrend.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.obitrend.app.Activities.MissingPersonItem;
import com.obitrend.app.Activities.PublicNoticeItem;
import com.obitrend.app.R;
import com.obitrend.app.adapters.MissingPersonsAdapter;
import com.obitrend.app.adapters.PublicNoticeAdapter;
import com.obitrend.app.models.MissingPerson;
import com.obitrend.app.models.PublicNotice;
import com.obitrend.app.utils.AppConstants;
import com.obitrend.app.utils.RecyclerItemClickListener;

/**
 * Created by ekirapa on 4/9/18.
 * Missing persons fragment class
 */

public class MissingPersonsFrag extends Fragment {
    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_layout, container, false);
        registerViews();
        return v;
    }

    private void registerViews() {
        RecyclerView recyclerView = v.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        final MissingPersonsAdapter adapter = new MissingPersonsAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MissingPerson person = adapter.getPerson(position);
                Intent intent = new Intent(getContext(), MissingPersonItem.class);
                Bundle b = new Bundle();
                b.putParcelable(AppConstants.MISSING_PERSON, person);
                intent.putExtras(b);
                startActivity(intent);
            }
        }));
    }
}
