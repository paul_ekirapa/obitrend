package com.obitrend.app.models;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by ekirapa on 1/2/18.
 * Database model
 */

@Database(version = AppDatabase.VERSION)
public class AppDatabase {
    public static final int VERSION = 1;
}
