package com.obitrend.app.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by ekirapa on 4/19/18.
 * <p>
 * User model class
 */
@Table(database = AppDatabase.class, name = "user")
public class User extends BaseModel {
    @PrimaryKey
    @Column
    private String first_name;

    @Column
    private String other_names;

    @Column
    private String phone_number;

    @Column
    private String my_country = "KE";

    @Column
    private String gender;

    @Column
    private String email;

    @Column
    private String password;

    public User() {

    }

    public User(String first_name, String other_names, String phone_number, String my_country, String gender, String email, String password) {
        setEmail(email);
        setFirst_name(first_name);
        setGender(gender);
        setMy_country(my_country);
        setOther_names(other_names);
        setPassword(password);
        setPhone_number(phone_number);
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getOther_names() {
        return other_names;
    }

    public void setOther_names(String other_names) {
        this.other_names = other_names;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMy_country() {
        return my_country;
    }

    public void setMy_country(String my_country) {
        this.my_country = my_country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
