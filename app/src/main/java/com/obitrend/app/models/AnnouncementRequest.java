package com.obitrend.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.File;


/**
 * Created by ekirapa on 4/19/18.
 * Announcements
 */

public class AnnouncementRequest extends BaseModel implements Parcelable {
    public AnnouncementRequest() {

    }

    private String user_id;

    private String type_of_announcement;

    private String location;

    private String description;

    private File image_path;

    private File image_thumb;

    private File file_path;

    private String payment;

    private String country = "KE";

    private String is_featured ="0";

    private String title;

    private String status = "0";

    private String is_featured_dirty = "0";

    private String days;

    private String remember_token;

    private String created_at;

    private String updated_at;




    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType_of_announcement() {
        return type_of_announcement;
    }

    public void setType_of_announcement(String type_of_announcement) {
        this.type_of_announcement = type_of_announcement;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getImage_path() {
        return image_path;
    }

    public void setImage_path(File image_path) {
        this.image_path = image_path;
    }

    public File getImage_thumb() {
        return image_thumb;
    }

    public void setImage_thumb(File image_thumb) {
        this.image_thumb = image_thumb;
    }

    public File getFile_path() {
        return file_path;
    }

    public void setFile_path(File file_path) {
        this.file_path = file_path;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_featured_dirty() {
        return is_featured_dirty;
    }

    public void setIs_featured_dirty(String is_featured_dirty) {
        this.is_featured_dirty = is_featured_dirty;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    protected AnnouncementRequest(Parcel in) {
        user_id = in.readString();
        type_of_announcement = in.readString();
        location = in.readString();
        description = in.readString();
        image_path = new File(in.readString());
        image_thumb = new File(in.readString());
        file_path = new File(in.readString());
        payment = in.readString();
        country = in.readString();
        is_featured = in.readString();
        title = in.readString();
        status = in.readString();
        is_featured_dirty = in.readString();
        days = in.readString();
        remember_token = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<AnnouncementRequest> CREATOR = new Creator<AnnouncementRequest>() {
        @Override
        public AnnouncementRequest createFromParcel(Parcel in) {
            return new AnnouncementRequest(in);
        }

        @Override
        public AnnouncementRequest[] newArray(int size) {
            return new AnnouncementRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(user_id);
        parcel.writeString(type_of_announcement);
        parcel.writeString(location);
        parcel.writeString(description);
        parcel.writeString(String.valueOf(image_path));
        parcel.writeString(String.valueOf(image_thumb));
        parcel.writeString(String.valueOf(file_path));
        parcel.writeString(payment);
        parcel.writeString(country);
        parcel.writeString(is_featured);
        parcel.writeString(title);
        parcel.writeString(status);
        parcel.writeString(is_featured_dirty);
        parcel.writeString(days);
        parcel.writeString(remember_token);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
    }
}
