package com.obitrend.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.obitrend.app.helpers.UrlMapper;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;


/**
 * Created by ekirapa on 4/19/18.
 * Announcements
 */

@Table(database = AppDatabase.class, name = "publicnotice")
public class PublicNotice extends BaseModel implements Parcelable {
    public PublicNotice() {

    }

    @Column
    @PrimaryKey
    private String id;

    @Column
    private String user_id;

    @Column
    private String type_of_announcement;

    @Column
    private String location;

    @Column
    private String description;

    @Column
    private String image_path;

    @Column
    private String image_thumb;

    @Column
    private String file_path;

    @Column
    private String payment;

    @Column
    private String country;

    @Column
    private String is_featured;

    @Column
    private String title;

    @Column
    private String status;

    @Column
    private String is_featured_dirty;

    @Column
    private String days;

    @Column
    private String remember_token;

    @Column
    private String created_at;

    @Column
    private String updated_at;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType_of_announcement() {
        return type_of_announcement;
    }

    public void setType_of_announcement(String type_of_announcement) {
        this.type_of_announcement = type_of_announcement;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = UrlMapper.imageUrlMapper(image_path);
    }

    public String getImage_thumb() {
        return image_thumb;
    }

    public void setImage_thumb(String image_thumb) {
        this.image_thumb = UrlMapper.imageUrlMapper(image_thumb);
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_featured_dirty() {
        return is_featured_dirty;
    }

    public void setIs_featured_dirty(String is_featured_dirty) {
        this.is_featured_dirty = is_featured_dirty;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    protected PublicNotice(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        type_of_announcement = in.readString();
        location = in.readString();
        description = in.readString();
        image_path = in.readString();
        image_thumb = in.readString();
        file_path = in.readString();
        payment = in.readString();
        country = in.readString();
        is_featured = in.readString();
        title = in.readString();
        status = in.readString();
        is_featured_dirty = in.readString();
        days = in.readString();
        remember_token = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<PublicNotice> CREATOR = new Creator<PublicNotice>() {
        @Override
        public PublicNotice createFromParcel(Parcel in) {
            return new PublicNotice(in);
        }

        @Override
        public PublicNotice[] newArray(int size) {
            return new PublicNotice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(user_id);
        parcel.writeString(type_of_announcement);
        parcel.writeString(location);
        parcel.writeString(description);
        parcel.writeString(image_path);
        parcel.writeString(image_thumb);
        parcel.writeString(file_path);
        parcel.writeString(payment);
        parcel.writeString(country);
        parcel.writeString(is_featured);
        parcel.writeString(title);
        parcel.writeString(status);
        parcel.writeString(is_featured_dirty);
        parcel.writeString(days);
        parcel.writeString(remember_token);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
    }
}
