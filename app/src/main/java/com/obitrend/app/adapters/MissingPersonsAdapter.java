package com.obitrend.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.obitrend.app.R;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.models.MissingPerson;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekirapa on 4/19/18.
 * Announcements recyclerview adapter class
 */

public class MissingPersonsAdapter extends RecyclerView.Adapter<MissingPersonsAdapter.AnnouncementsHolder> {
    private List<MissingPerson> missingPeople = new ArrayList<>();
    private Context context;

    public MissingPersonsAdapter(Context context) {
        this.context = context;
        try {
            missingPeople = SQLite.select()
                    .from(MissingPerson.class)
                    .queryList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public AnnouncementsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.missing_person_item, parent, false);
        return new AnnouncementsHolder(view);
    }

    @Override
    public void onBindViewHolder(AnnouncementsHolder holder, int position) {
        MissingPerson person = missingPeople.get(position);
        holder.date.setText(person.getCreated_at());
        holder.description.setText(person.getDescription());
        holder.title.setText(person.getTitle());
        PicasoHelper.getPicaso(context).load(person.getImage_thumb()).placeholder(R.drawable.brainstorm).fit().centerCrop().into(holder.imageView);
    }

    public MissingPerson getPerson(int position) {
        return missingPeople.get(position);
    }

    @Override
    public int getItemCount() {
        return missingPeople.size();
    }

    class AnnouncementsHolder extends RecyclerView.ViewHolder {
        private TextView date, title, description;
        private ImageView imageView;

        public AnnouncementsHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            title = itemView.findViewById(R.id.tv_title);
            description = itemView.findViewById(R.id.tv_description);
            imageView = itemView.findViewById(R.id.img_pic);
        }
    }
}
