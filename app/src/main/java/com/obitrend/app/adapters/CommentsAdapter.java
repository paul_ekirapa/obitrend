package com.obitrend.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.obitrend.app.R;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.models.Comments;
import com.obitrend.app.models.Comments_Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekirapa on 5/3/18.
 * Comments adapter
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsHolder> {
    private Context context;
    private List<Comments> comments = new ArrayList<>();

    public CommentsAdapter(Context context, String id) {
        this.context = context;
        try {
            comments = SQLite.select()
                    .from(Comments.class)
                    .where(Comments_Table.announcement_id.eq(id))
                    .queryList();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public CommentsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_comment_item, parent, false);
        return new CommentsHolder(v);
    }

    @Override
    public void onBindViewHolder(CommentsHolder holder, int position) {
        Comments comment = comments.get(position);
        holder.comment.setText(comment.getComment());
        holder.user.setText(comment.getUser_name());
    }

    @Override
    public int getItemCount() {
        if (comments.size() > 3) {
            return 3;
        }
        return comments.size();
    }

    class CommentsHolder extends RecyclerView.ViewHolder {
        TextView comment, user;

        public CommentsHolder(View itemView) {
            super(itemView);
            comment = itemView.findViewById(R.id.tv_comment);
            user = itemView.findViewById(R.id.tv_comment_user);
        }
    }
}
