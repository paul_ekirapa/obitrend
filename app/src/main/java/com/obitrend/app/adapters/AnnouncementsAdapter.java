package com.obitrend.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.Announcement;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ekirapa on 4/19/18.
 * Announcements recyclerview adapter class
 */

public class AnnouncementsAdapter extends RecyclerView.Adapter<AnnouncementsAdapter.AnnouncementsHolder> {
    private List<Announcement> announcements = new ArrayList<>();
    private Context context;

    public AnnouncementsAdapter(Context context) {
        this.context = context;
        try {
            announcements = SQLite.select()
                    .from(Announcement.class)
                    .queryList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public AnnouncementsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.announcement_item, parent, false);
        return new AnnouncementsHolder(view);
    }

    @Override
    public void onBindViewHolder(AnnouncementsHolder holder, int position) {
        Announcement announcement = announcements.get(position);
        holder.date.setText(announcement.getCreated_at());
        holder.description.setText(announcement.getDescription());
        holder.title.setText(announcement.getTitle());
        //Picasso.with(context).load(announcement.getImage_path()).placeholder(R.drawable.brainstorm).fit().centerCrop().into(holder.imageView);

        PicasoHelper.getPicaso(context).load(announcement.getImage_thumb()).placeholder(R.drawable.brainstorm).fit().centerCrop().into(holder.imageView);
    }

    public Announcement getAnnouncement(int position) {
        return announcements.get(position);
    }

    @Override
    public int getItemCount() {
        return announcements.size();
    }

    class AnnouncementsHolder extends RecyclerView.ViewHolder {
        private TextView date, title, description;
        private ImageView imageView;

        public AnnouncementsHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            title = itemView.findViewById(R.id.tv_title);
            description = itemView.findViewById(R.id.tv_description);
            imageView = itemView.findViewById(R.id.img_pic);
        }
    }
}
