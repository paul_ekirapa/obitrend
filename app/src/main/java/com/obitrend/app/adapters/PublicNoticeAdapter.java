package com.obitrend.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.obitrend.app.R;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.PublicNotice;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekirapa on 4/19/18.
 * Announcements recyclerview adapter class
 */

public class PublicNoticeAdapter extends RecyclerView.Adapter<PublicNoticeAdapter.NoticeHolder> {
    private List<PublicNotice> notices = new ArrayList<>();
    private Context context;

    public PublicNoticeAdapter(Context context) {
        this.context = context;
        try {
            notices = SQLite.select()
                    .from(PublicNotice.class)
                    .queryList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public NoticeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.public_notice_item, parent, false);
        return new NoticeHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticeHolder holder, int position) {
        PublicNotice publicNotice = notices.get(position);
        holder.date.setText(publicNotice.getCreated_at());
        holder.description.setText(publicNotice.getDescription());
        holder.title.setText(publicNotice.getTitle());
        PicasoHelper.getPicaso(context).load(publicNotice.getImage_thumb()).placeholder(R.drawable.brainstorm).fit().centerCrop().into(holder.imageView);
    }

    public PublicNotice getNotice(int position) {
        return notices.get(position);
    }

    @Override
    public int getItemCount() {
        return notices.size();
    }

    class NoticeHolder extends RecyclerView.ViewHolder {
        private TextView date, title, description;
        private ImageView imageView;

        public NoticeHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            title = itemView.findViewById(R.id.tv_title);
            description = itemView.findViewById(R.id.tv_description);
            imageView = itemView.findViewById(R.id.img_pic);
        }
    }
}
