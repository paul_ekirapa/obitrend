package com.obitrend.app.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.adapters.AnnouncementsAdapter;
import com.obitrend.app.adapters.CommentsAdapter;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.models.Comments;
import com.obitrend.app.utils.AppConstants;
import com.obitrend.app.utils.RecyclerItemClickListener;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ru.egslava.blurredview.BlurredImageView;

/**
 * Created by ekirapa on 4/21/18.
 * Announcement item class
 */

public class AnnouncementItem extends AbstractItemActivity {
    BlurredImageView imageView;
    CircularImageView avatar;
    private Announcement announcement;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    CommentsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcemnt);

        TextView title = findViewById(R.id.tv_title);
        TextView description = findViewById(R.id.tv_description);
        TextView location = findViewById(R.id.tv_location);
        TextView country = findViewById(R.id.tv_country);
        TextView days = findViewById(R.id.tv_days);
        recyclerView = findViewById(R.id.recycler_comments);
        setToolbar();

        imageView = findViewById(R.id.header_image);
        avatar = findViewById(R.id.avatar);
        Bundle b = this.getIntent().getExtras();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fetching comments and tributes...");
        progressDialog.setCancelable(false);

        if (b != null) {
            announcement = b.getParcelable(AppConstants.ANNOUNCEMENT);
            title.setText(announcement != null ? announcement.getTitle() : null);
            description.setText(announcement != null ? announcement.getDescription() : null);
            PicasoHelper.getPicaso(getBaseContext()).load(announcement != null ? announcement.getImage_thumb() : null).placeholder(R.drawable.brainstorm).fit().centerCrop().into(imageView);
            PicasoHelper.getPicaso(getBaseContext()).load(announcement != null ? announcement.getImage_thumb() : null).placeholder(R.drawable.brainstorm).fit().centerCrop().into(avatar);
            location.setText(announcement != null ? WordUtils.capitalizeFully(announcement.getLocation()) : null);
            country.setText(announcement != null ? announcement.getCountry() : null);
            days.setText(announcement != null ? announcement.getDays() + " days" : null);

            // setRecycler();
            fetchComments(announcement.getId());
        }


    }

    private void setRecycler() {
        adapter = new CommentsAdapter(getBaseContext(), announcement.getId());
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        }));
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public void shareMenuItemClickedd() {
        Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
        if (announcement != null) {
            shareItem(announcement.getDescription());
        }
    }

    public void shareItem(String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain*");

        // Launch sharing dialog for image
        startActivity(Intent.createChooser(shareIntent,
                "Share Announcement Via.."));
    }

    public void ShareItem(String text, String name) {
        // Get access to the URI for the bitmap
        Uri bmpUri = getLOcalBitmap(imageView, name);
        if (bmpUri != null) {

            // Construct a ShareIntent with link to image

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("text/plain*");

            // Launch sharing dialog for image
            startActivity(Intent.createChooser(shareIntent,
                    "Share Announcement Via.."));

        } else {
            // ...sharing failed, handle error
            Toast.makeText(getBaseContext(), "Can not share announcement  now",
                    Toast.LENGTH_LONG).show();

        }

    }

    private Uri getLOcalBitmap(BlurredImageView image, String name) {
        Drawable drawable = image.getDrawable();
        Bitmap b = null;
        if (drawable instanceof BitmapDrawable) {
            b = ((BitmapDrawable) image.getDrawable()).getBitmap();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b,
                    name, "Image");
            Uri uri = Uri.parse(path);
            return uri;
        } else {
            return null;

        }

    }

    private void fetchComments(final String id) {
        progressDialog.show();
        final SessionManager sessionManager = new SessionManager(getBaseContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, String.format(UrlConfig.GET_COMMENTS, id), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response.toString());
                if (response.has("success")) {
                    try {
                        JSONArray array = response.getJSONArray("tributes");
                        Log.d("Trubutes", array.toString());
                        saveComments(array);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        closeProgressbar();
                    }


                } else {
                    closeProgressbar();
                    Toast.makeText(AnnouncementItem.this, "Unable to ftch data now", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                closeProgressbar();
                Toast.makeText(AnnouncementItem.this, "Unable to ftch data now", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                return headers;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void saveComments(JSONArray array) {
        Gson gson = new Gson();
        for (int i = 0; i < array.length(); i++) {
            try {
                Comments comment = gson.fromJson(array.getJSONObject(i).toString(), Comments.class);
                JSONObject jsonObject = array.getJSONObject(i);
                JSONObject user = jsonObject.getJSONObject("user");
                if (user != null) {
                    String firstname = user.getString("first_name");
                    String last_name = user.getString("other_names");
                    comment.setUser_name(firstname + " " + last_name);
                    Log.d("Saving", comment.getUser_name());

                } else {
                    comment.setUser_name("User Name");

                }
                //
                Log.d("User", user.toString());
                Log.d("json", jsonObject.toString());
                comment.save();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        closeProgressbar();
        setRecycler();
    }
}
