package com.obitrend.app.Activities;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.obitrend.app.R;

/**
 * Created by ekirapa on 4/23/18.
 * Abstract class for item  activity
 */

@SuppressLint("Registered")
abstract class AbstractItemActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:

                shareMenuItemClickedd();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public abstract void shareMenuItemClickedd();
}
