package com.obitrend.app.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.adapters.BottomBarAdapter;
import com.obitrend.app.fragments.AnnouncementsFrag;
import com.obitrend.app.fragments.MissingPersonsFrag;
import com.obitrend.app.fragments.PublicNoticeFrag;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.Account;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.models.MissingPerson;
import com.obitrend.app.models.PublicNotice;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;
import com.obitrend.app.widgets.FragViewPager;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private BottomBarAdapter adapter;
    private AHBottomNavigation bottomNavigation;
    private boolean tabNotification1 = false;
    private boolean tabNotification2 = false;
    private boolean tabNotification3 = false;
    private FragViewPager viewPager;
    private int currentPosition = 0;
    private ProgressDialog progressDialog;
    private DrawerLayout drawerLayout;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    private AHBottomNavigation.OnTabSelectedListener tabSelectedListener = new AHBottomNavigation.OnTabSelectedListener() {
        @Override
        public boolean onTabSelected(int position, boolean wasSelected) {

            viewPager.setCurrentItem(position);
            return true;
        }
    };
    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setPagingEnabled(false);
        //setupViewPager();
        setDrawer();
        setupBottomNav();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fetching announcements...");
        progressDialog.setCancelable(false);
        getAnnouncements();

    }

    private void setupViewPager() {
        adapter = new BottomBarAdapter(getSupportFragmentManager());
        adapter.addFragments(new AnnouncementsFrag());
        adapter.addFragments(new MissingPersonsFrag());
        adapter.addFragments(new PublicNoticeFrag());

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        currentPosition = 0;
    }

    private void setupBottomNav() {
        bottomNavigation = findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.death_announcement), R.drawable.ic_announcement_black_24dp);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(getString(R.string.missing_person), R.drawable.ic_person_black_24dp);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(getString(R.string.public_notice), R.drawable.ic_notifications_black_24dp);

        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.setOnTabSelectedListener(tabSelectedListener);
        bottomNavigation.setBehaviorTranslationEnabled(true);
        //bottomNavigation.setCurrentItem(0);
    }

    private void setDrawer() {
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.drawer_home:
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.drawer_request:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(getBaseContext(), MakeRequest.class));
                        break;
                    case R.id.drawer_contacts:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(getBaseContext(), ContactUs.class));
                        break;
                    case R.id.drawer_about_us:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(getBaseContext(), AboutUs.class));
                        break;
                }
                return false;
            }
        });

        View v = navigationView.getHeaderView(0);
        TextView user_name = v.findViewById(R.id.tv_username);
        CircularImageView avatar = v.findViewById(R.id.avatar);
        Account account = SQLite.select()
                .from(Account.class)
                .querySingle();
        if (account != null) {
            user_name.setText(account.getFirst_name() + " " + account.getOther_names());
            Log.d("Account avatar", account.getAvatar());
            PicasoHelper.getPicaso(getBaseContext()).load(account != null ? account.getAvatar() : null).placeholder(R.drawable.user).fit().centerCrop().into(avatar);

        }
    }

    private void getAnnouncements() {
        progressDialog.show();
        final SessionManager sessionManager = new SessionManager(getBaseContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlConfig.GET_ANNOUNCEMENTS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response.toString());
                if (response.has("success")) {
                    try {
                        JSONObject data = response.getJSONObject("data");
                        JSONArray announcements = data.getJSONArray("announcements");
                        JSONArray publicNotices = data.getJSONArray("public");
                        JSONArray missingPersons = data.getJSONArray("missing");
                        saveAnnouncements(announcements);
                        savePublicNotices(publicNotices);
                        saveMissingPersons(missingPersons);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    closeProgressbar();

                } else {
                    closeProgressbar();
                    Toast.makeText(MainActivity.this, "Unable to ftch data now", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                closeProgressbar();
                Toast.makeText(MainActivity.this, "Unable to ftch data now", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                return headers;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        setupViewPager();
    }

    private void saveAnnouncements(JSONArray array) {
        Gson gson = new Gson();
        for (int i = 0; i < array.length(); i++) {
            try {
                Announcement announcement = gson.fromJson(array.getJSONObject(i).toString(), Announcement.class);
                announcement.save();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void savePublicNotices(JSONArray array) {
        Gson gson = new Gson();
        for (int i = 0; i < array.length(); i++) {
            try {
                PublicNotice publicNotice = gson.fromJson(array.getJSONObject(i).toString(), PublicNotice.class);
                publicNotice.save();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveMissingPersons(JSONArray array) {
        Gson gson = new Gson();
        for (int i = 0; i < array.length(); i++) {
            try {
                MissingPerson missingPerson = gson.fromJson(array.getJSONObject(i).toString(), MissingPerson.class);
                missingPerson.save();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
