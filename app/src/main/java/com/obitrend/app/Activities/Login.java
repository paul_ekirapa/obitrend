package com.obitrend.app.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.models.Account;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;
import com.raizlabs.android.dbflow.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ekirapa on 4/13/18.
 * Login activity
 */

public class Login extends AppCompatActivity {
    private EditText et_email, et_password;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button submit = findViewById(R.id.btn_submit);
        et_email = findViewById(R.id.et_user_name);
        et_password = findViewById(R.id.et_password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Logging in...");
        progressDialog.setCancelable(false);
        TextView signup = findViewById(R.id.tv_no_account);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check()) {
                    try {
                        login(et_email.getText().toString(), et_password.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), SignUp.class);
                startActivity(intent);
            }
        });
    }

    private boolean check() {
        if (StringUtils.isNullOrEmpty(et_email.getText().toString())) {
            et_email.setError(getString(R.string.error_email));
            return false;
        } else if (StringUtils.isNullOrEmpty(et_password.getText().toString())) {
            et_password.setError(getString(R.string.error_no_password));
            return false;
        }
        return true;
    }

    private void login(final String email, final String password) throws JSONException {
        progressDialog.show();
        final SessionManager sessionManager = new SessionManager(getBaseContext());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);
        jsonObject.put("password", password);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlConfig.LOGIN_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response.has("success")) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        Account account = gson.fromJson(response.getJSONObject("success").getJSONObject("user").toString(), Account.class);
                        account.save();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finishLogin();
                } else {
                    closeProgressbar();
                    Toast.makeText(Login.this, "Cannot Log in at this time", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                closeProgressbar();
                if (error instanceof AuthFailureError) {
                    Toast.makeText(Login.this, "Wrong email and password combination", Toast.LENGTH_SHORT).show();
                    et_password.setError("Wrong password");
                }
                Toast.makeText(Login.this, "Cannot Log in at this time", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                return headers;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private void finishLogin() {
        closeProgressbar();
        new SessionManager(getBaseContext()).setLoggedIn(true);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
