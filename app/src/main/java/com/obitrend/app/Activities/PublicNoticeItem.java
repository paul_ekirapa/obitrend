package com.obitrend.app.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.obitrend.app.R;
import com.obitrend.app.helpers.PicasoHelper;
import com.obitrend.app.models.Announcement;
import com.obitrend.app.models.PublicNotice;
import com.obitrend.app.utils.AppConstants;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.text.WordUtils;

import ru.egslava.blurredview.BlurredImageView;

/**
 * Created by ekirapa on 4/21/18.
 * Announcement item class
 */

public class PublicNoticeItem extends AbstractItemActivity {
    BlurredImageView imageView;
    CircularImageView avatar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_notice);

        TextView title = findViewById(R.id.tv_title);
        TextView description = findViewById(R.id.tv_description);
        TextView location = findViewById(R.id.tv_location);
        TextView country = findViewById(R.id.tv_country);
        TextView days = findViewById(R.id.tv_days);
        setToolbar();

        imageView = findViewById(R.id.header_image);
        avatar = findViewById(R.id.avatar);
        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            PublicNotice publicNotice = b.getParcelable(AppConstants.PUBLIC_NOTICE);
            title.setText(publicNotice != null ? publicNotice.getTitle() : null);
            description.setText(publicNotice != null ? publicNotice.getDescription() : null);
            PicasoHelper.getPicaso(getBaseContext()).load(publicNotice != null ? publicNotice.getImage_thumb() : null).placeholder(R.drawable.brainstorm).fit().centerCrop().into(imageView);
            PicasoHelper.getPicaso(getBaseContext()).load(publicNotice != null ? publicNotice.getImage_thumb() : null).placeholder(R.drawable.brainstorm).fit().centerCrop().into(avatar);
            location.setText(publicNotice != null ? WordUtils.capitalizeFully(publicNotice.getLocation()) : null);
            country.setText(publicNotice != null ? publicNotice.getCountry() : null);
            days.setText(publicNotice != null ? publicNotice.getDays() + " days" : null);
        }
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void shareMenuItemClickedd() {

    }
}
