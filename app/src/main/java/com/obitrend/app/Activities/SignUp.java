package com.obitrend.app.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.obitrend.app.R;
import com.obitrend.app.SessionManager;
import com.obitrend.app.models.Account;
import com.obitrend.app.models.User;
import com.obitrend.app.utils.UrlConfig;
import com.obitrend.app.utils.VolleySingleton;
import com.raizlabs.android.dbflow.config.FlowManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ekirapa on 4/23/18.
 * Sign up activity class.
 */

public class SignUp extends AppCompatActivity {
    EditText first_name, other_name, phone, country, email, password, confirm;
    String gender = "";
    private ProgressDialog progressDialog;
    Button submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        first_name = findViewById(R.id.et_first_name);
        other_name = findViewById(R.id.et_other_names);
        email = findViewById(R.id.et_email);
        phone = findViewById(R.id.et_phone_number);
        password = findViewById(R.id.et_password);
        submit = findViewById(R.id.btn_submit);
        confirm = findViewById(R.id.et_confirm_password);

        RadioGroup radioGroup = findViewById(R.id.radio_gender);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = radioGroup.findViewById(i);
                if (radioButton.getText().toString().equals("Male")) {
                    gender = "1";
                } else {
                    gender = "1";
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkDetails()) {
                    User user = new User();
                    user.setPhone_number(phone.getText().toString());
                    user.setPassword(password.getText().toString());
                    user.setOther_names(other_name.getText().toString());
                    user.setGender(gender);
                    user.setFirst_name(first_name.getText().toString());
                    user.setEmail(email.getText().toString());
                    signUp(user);
                }
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Signing Up...");
        progressDialog.setCancelable(false);

    }

    private boolean checkDetails() {
        if (first_name.getText().toString().isEmpty()) {
            first_name.setError(getString(R.string.first_name_empty_error));
            return false;
        } else if (other_name.getText().toString().isEmpty()) {
            first_name.setError(getString(R.string.other_name_empty_error));
            return false;
        } else if (email.getText().toString().isEmpty()) {
            email.setError(getString(R.string.error_email));
            return false;
        } else if (phone.getText().toString().isEmpty()) {
            phone.setError(getString(R.string.error_non_empty));
            return false;
        } else if (password.getText().toString().isEmpty()) {
            password.setError(getString(R.string.error_non_empty));
            return false;
        } else if (confirm.getText().toString().isEmpty()) {
            confirm.setError(getString(R.string.error_non_empty));
            return false;
        } else if (!password.getText().toString().equals(confirm.getText().toString())) {
            confirm.setError(getString(R.string.error_password_matching));
            return false;
        }
        if (gender.isEmpty()) {
            Toast.makeText(getBaseContext(), "Choose your gender", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void signUp(final User user) {
        final Gson gson = new GsonBuilder().create();
        final SessionManager sessionManager = new SessionManager(getBaseContext());
        Log.d("Session token", sessionManager.getKeyBearerToken());
        progressDialog.show();

        try {
            JSONObject jsonObject = new JSONObject(gson.toJson(user));
            Log.d("JSON", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlConfig.USER_REGISTER_URL, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response.toString());
                    if (response.has("success")) {
                        user.save();
                        try {
                            Account account = gson.fromJson(response.getJSONObject("success").getJSONObject("user").toString(), Account.class);
                            account.save();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    finishLogin();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    closeProgressbar();
                    Toast.makeText(getBaseContext(), "Cant sign up.Try again", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + sessionManager.getKeyBearerToken());
                    return headers;
                }
            };
            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            closeProgressbar();
        }

    }

    private void finishLogin() {
        closeProgressbar();
        new SessionManager(getBaseContext()).setLoggedIn(true);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void closeProgressbar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
