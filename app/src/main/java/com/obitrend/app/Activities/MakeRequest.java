package com.obitrend.app.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.obitrend.app.R;
import com.obitrend.app.fragments.AnnouncementDetails;
import com.obitrend.app.fragments.AnnouncementFiles;
import com.obitrend.app.fragments.Payments;
import com.obitrend.app.models.AnnouncementRequest;
import com.obitrend.app.utils.FragmentModel;

/**
 * Created by ekirapa on 5/3/18.
 * Make request class
 */

public class MakeRequest extends AppCompatActivity implements FragmentModel.FragStateChangeListener {
    private FragmentTransaction transaction;
    private AnnouncementRequest request = new AnnouncementRequest();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        transaction = getSupportFragmentManager().beginTransaction();
        setToolbar();

        if (savedInstanceState == null) {
            transaction.add(R.id.frame, new AnnouncementDetails()
                    , "Details").commit();

        }
        FragmentModel.getInstance().setListener(this);
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public AnnouncementRequest getRequest() {
        return this.request;
    }

    @Override
    public void nextFrag(int number) {
        transaction = getSupportFragmentManager().beginTransaction();
        switch (number) {
            case 0:
                transaction.replace(R.id.frame, new AnnouncementFiles(), "Get Files").commit();
                break;
            case 1:
                transaction.replace(R.id.frame, new Payments(), "Get Payment").commit();
                break;
        }
    }
}
