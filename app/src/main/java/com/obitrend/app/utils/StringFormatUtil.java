package com.obitrend.app.utils;

import android.text.Html;
import android.text.Spanned;

/**
 * Created by ekirapa on 5/11/18.
 * String utils
 */

public class StringFormatUtil {
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            if (html == null) return null;
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            if (html == null) return null;
            result = Html.fromHtml(html);
        }
        return result;
    }
}
