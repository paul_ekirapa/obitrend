package com.obitrend.app.utils;

/**
 * Created by ekirapa on 4/19/18.
 * Class with API urls constants
 */

public class UrlConfig {
    private static final String IP = "http://obitrend.com/";

    public static final String TOKEN_URL = IP + "oauth/token";
    public static final String LOGIN_URL = IP + "api/login";
    public static final String GET_ANNOUNCEMENTS = IP + "api/get-user";
    public static  final String USER_REGISTER_URL = IP + "api/register";
    public  static final String GET_COMMENTS = IP + "api/get/each/" + "%s";
    public static final String MAKE_REQUEST = IP + "api/create/announcement";
}
